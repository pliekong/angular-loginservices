import { Component, OnInit } from '@angular/core';

//add import
import { FormGroup } from '@angular/forms';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private user: any = {};

  constructor(private usersService: UsersService) {}

  ngOnInit() {

    this.user = {
      email: '',
      password: ''
    };
  }

  async signin(form: FormGroup) {
    //console.log(form.value);

    if(form.valid) {
      const response = await this.usersService.login(this.user).toPromise();
      console.log(response);
    } else {

      console.log('email or password invalid!!');
    }
  }
}
