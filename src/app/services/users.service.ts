import { Injectable } from '@angular/core';

// add import
import { HttpClient, HttpHeaders } from '@angular/common/http';

const ApiRoutes = {
  login: 'login',
  signup: 'signup',
  users: 'users'
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private UriApi: string = 'http://127.0.0.1:9000';
  constructor(private http: HttpClient) { }

  public login(user: any) {

    let uri = `${this.UriApi}/${ApiRoutes.login}`;
    return this.http.post(uri, JSON.stringify(user), this.loadHeaders());
  }

  private loadHeaders(token: string = '') {
    let headers = new HttpHeaders({
      'Content-type': 'application/json',
      'Authorization': `${token}`
    });

    return { headers };

  }
}
